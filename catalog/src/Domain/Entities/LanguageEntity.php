<?php

namespace Osc\Domain\Entities;

class LanguageEntity {

  private $id;
  private $name;
  private $image;
  private $code;
  private $dir;

    public function getId() {
        return $this->id;
    }

    public function getName() {
        return $this->name;
    }

    public function getImage() {
        return $this->image;
    }

    public function getCode() {
        return $this->code;
    }

    public function getDirectory() {
        return $this->dir;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function setImage($image) {
        $this->image = $image;
    }

    public function setCode($code) {
        $this->code = $code;
    }

    public function setDir($dir) {
        $this->dir = $dir;
    }

}