<?php

namespace Osc\Domain\Entities;

class CategoryEntity {

private $id;
private $name;
private $sortOrder;
private $image;
private $parent_id;
private $dateAdded;
private $dateModified;

public function getId() {
    return $this->id;
}

public function getName() {
    return $this->name;
}

public function getImage() {
    return $this->image;
}

public function getParentId() {
    return $this->parent_id;
}

public function getSortOrder() {
    return $this->sortOrder;
}

public function getDateAdded() {
    return $this->dateAdded;
}

public function getDateModified() {
    return $this->dateModified;
}

public function setId($id) {
    $this->id = $id;
}

public function setName($name) {
    $this->name = $name;
}

public function setImage($image) {
    $this->image = $image;
}

public function setParentId($parent_id) {
    $this->parent_id = $parent_id;
}

public function setSortOrder($sort_order) {
    $this->sortOrder = $sort_order;
}

public function setDateAdded($date_added) {
    $this->dateAdded = $date_added;
}

public function setDateModified($date_modified) {
    $this->dateModified = $date_modified;
}

}