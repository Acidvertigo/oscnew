<?php

namespace Osc\Domain\Mappers;

use Osc\Services\Persistence\Db\LanguageDAO as DAO;
use Osc\Domain\Entities\LanguageEntity;

/**
 * class Language
 */
class LanguageMapper
{
    /**
     * @var DAO $adapter
     */
    private $adapter;

    /**
     * @param DAO $dbLayer
     */
    public function __construct(DAO $dbLayer)
    {
        $this->adapter = $dbLayer;
    }

    /**
     * @param Language $language
     * @return bool
     */
    public function save(Language $language)
    {
        /* $data keys should correspond to valid Table columns on the Database */
        $data[$language->getCode()] = [
            'id'        => $language->getId(),
            'name'      => $language->getName(),
            'image'     => $language->getImage(),
            'code'      => $language->getCode(),
            'directory' => $language->getDir()
        ];

        // if no ID specified create new language object else update the one in the Database
        $id = $language->getId();

        if (null === $id) {
            unset($data['id']);
            $this->adapter->insert($data);

            return true;
        } else {
            $this->adapter->update($data, array('language_id = ?' => $id));

            return true;
        }
    }

    /**
     * @param int $id
     * @throws \InvalidArgumentException
     * @return LanguageEntity
     */
    public function findOne($id)
    {
        $result = $this->adapter->find($id);

        if (0 == count($result)) {
            throw new \InvalidArgumentException("Language id: $id not found");
        }
        $row = $result->current();

        return $this->mapObject($row);
    }

    /**
     * @return array
     */
    public function findAll()
    {
        $resultSet = $this->adapter->findAll();
        $entries = array();

        foreach ($resultSet as $row) {
            $entries[$row['code']] = $this->mapObject($row);
        }

        return $entries;
    }

    /**
     * @param array $row
     * @return LanguageEntity
     */
    protected function mapObject(array $row)
    {
        $language = new LanguageEntity;
        $language->setId($row['languages_id']);
        $language->setName($row['name']);
        $language->setCode($row['code']);
        $language->setImage($row['image']);
        $language->setDir($row['directory']);

        return $language;
    }
}