<?php

namespace Osc\Domain\Mappers;

interface MapperInterface {

public function findOne ($id);

public function findAll();

}