<?php

namespace Osc\Domain\Mappers;

use Osc\Services\Persistence\Db\CategoryDAO as DAO;
use Osc\Domain\Entities\CategoryEntity;

class CategoryMapper implements MapperInterface {

private $dbLayer;
private $cPath_array;

    /**
     * @var DAO $adapter
     */
    private $adapter;
    
    private $relatesWith;

    /**
     * @param DAO $dbLayer
     */
    public function __construct(DAO $dbLayer)
    {
        $this->adapter = $dbLayer;
        $this->relatesWith = [TABLE_CATEGORIES_DESCRIPTION];
    }

    /**
     * @param Category $category
     * @return bool
     */
    public function save(Category $category)
    {
        /* $data keys should correspond to valid Table columns on the Database */
        $data[$category->getId()] = [
            'id'            =>     $category->getId(),
            'name'          =>     $category->getName(),
            'parent_id'     =>     $category->getParentId(),
            'sort_order'    =>     $category->getSortOrder(),
            'date_added'    =>     $category->getDateAdded(),
            'last_modified' =>     $category->getDateModified()
        ];

        // if no ID specified create new language object else update the one in the Database
        $id = $category->getId();

        if (null === $id) {
            unset($data['id']);
            $this->adapter->insert($data);

            return true;
        } else {
            $this->adapter->update($data, array('categories_id = ?' => $id));

            return true;
        }
    }

    /**
     * @param int $id
     * @throws \InvalidArgumentException
     * @return CategoryEntity
     */
    public function findOne($id)
    {
        $result = $this->adapter->find($id);

        if (0 == count($result)) {
            throw new \InvalidArgumentException("Category id: $id not found");
        }
        $row = $result->current();

        return $this->mapObject($row);
    }

    /**
     * @return array
     */
    public function findAll()
    {
        $resultSet = $this->adapter->findAll();
        $entries = array();

        foreach ($resultSet as $row) {
            $entries[$row['code']] = $this->mapObject($row);
        }

        return $entries;
    }
    
    public function countRow($id) {
        return $this->adapter->countRow($id);
    }

    /**
     * @param array $row
     * @return CategoryEntity
     */
    protected function mapObject(array $row)
    {
        $category = new CategoryEntity;
        $category->setId($row['categories_id']);
        $category->setName($row['categories_name']);
        $category->setParentId($row['parent_id']);
        $category->setSortOrder($row['sort_order']);
        $category->setDateAdded($row['date_added']);
        $category->setDateModified($row['last_modified']);

        return $category;
    }

    public function getCurrentCategory($cPath = 0) {
        $this->cPath_array = $this->tep_parse_category_path($cPath);
        return end($this->cPath_array);
    }

    function buildTree($array, $parent_id = 0) {

         $array = array_combine(array_column($array, 'categories_id'), array_values($array));

        foreach ($array as $key => &$value) {
            if (isset($array[$value['parent_id']])) {
                 $array[$value['parent_id']]['children'][$key] = &$value;
            }
        unset($value);
        }
        return array_filter($array, function($value) use ($parent_id) {
            return $value['parent_id'] == $parent_id;
        });
   }


////
// Generate a path to categories
  public function tep_get_path($current_category_id = '') {

      if (tep_not_null($current_category_id)) {
      $cp_size = sizeof($this->cPath_array);
      if ($cp_size == 0) {
        $cPath_new = $current_category_id;
      } else {
        $cPath_new = '';
        $last_category_query = $this->db->query("select parent_id from " . TABLE_CATEGORIES . " where categories_id = '" . (int)$this->cPath_array[($cp_size - 1)] . "'");
        $last_category = $last_category_query->fetch();
        $current_category_query = $this->db->query("select parent_id from " . TABLE_CATEGORIES . " where categories_id = '" . (int)$current_category_id . "'");
        $current_category = $current_category_query->fetch();
        if ($last_category['parent_id'] == $current_category['parent_id']) {
          for ($i = 0; $i < ($cp_size - 1); $i++) {
            $cPath_new .= '_' . $this->cPath_array[$i];
          }
        } else {
          for ($i = 0; $i < $cp_size; $i++) {
            $cPath_new .= '_' . $this->cPath_array[$i];
          }
        }
        $cPath_new .= '_' . $current_category_id;
        if (substr($cPath_new, 0, 1) == '_') {
          $cPath_new = substr($cPath_new, 1);
        }
      }
    } else {
      $cPath_new = implode('_', $this->cPath_array);
    }

    return $cPath_new;
  }
  
  // Parse and secure the cPath parameter values

  /**
   * @param integer $cPath
   */
  public function tep_parse_category_path($cPath) {
// make sure the category IDs are integers
    $this->cPath_array = array_map(intval, explode('_', $cPath));

// make sure no duplicate category IDs exist which could lock the server in a loop
    $tmp_array = array();
    $n = sizeof($this->cPath_array);
    for ($i = 0; $i < $n; $i++) {
      if (!in_array($this->cPath_array[$i], $tmp_array)) {
        $tmp_array[] = $this->cPath_array[$i];
      }
    }

    return $tmp_array;
  }



}