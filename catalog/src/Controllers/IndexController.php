<?php

namespace Osc\Controllers;

use Zend\Diactoros\ServerRequest as Request;
use Zend\Diactoros\Response as Response;
use Osc\Services\Template\Template;
use Osc\Services\i18n\Language;
use Osc\Services\Message\MessageStack;
use Osc\Services\Core\ShoppingCart;
use Osc\Services\Currencies\Currencies;
use Osc\Domain\Mappers\CategoryMapper;

class IndexController {
    private $request;
    private $response;
    private $template;
    private $language;
    private $messageStack;
    private $currencies;
    private $category;
    private $cart;

    public function __construct(Request $request, Response $response, Template $template, Language $language, MessageStack $messageStack, ShoppingCart $cart, CategoryMapper $category, Currencies $currencies) {
        $this->request = $request;
        $this->response = $response;
        $this->template = $template;
        $this->language = $language;
        $this->messageStack = $messageStack;
        $this->cart = $cart;
        $this->currencies = $currencies;
        $this->category = $category;

        $cPath = $this->request->getQueryParams()['cPath'];
        $language = $this->language->get_language()->getDirectory();
        ob_start();
        require 'default.php';
        $content = ob_get_contents();
        ob_end_clean();     
        $this->response->getBody()->write($content);
    }

}