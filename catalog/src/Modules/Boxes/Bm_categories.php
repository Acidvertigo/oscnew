<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/
namespace Osc\Modules\Boxes;

use Osc\Modules\ModuleInterface;
use Osc\Services\Template\Template;
use Osc\Services\i18n\Language;

  class Bm_categories implements ModuleInterface {
    private $code = 'bm_categories';
    private $group = 'boxes';
    private $title;
    private $description;
    private $sort_order;
    private $enabled = false;
    private $template;
    private $tree = [];
    private $categories_string;
    private $language;
    private $cPath;
    private $cPath_array;

    public function __construct(Template $template, Language $language, $cPath = '', array $cPath_array = []) {
      $this->title = MODULE_BOXES_CATEGORIES_TITLE;
      $this->description = MODULE_BOXES_CATEGORIES_DESCRIPTION;
      $this->template = $template;
      $this->language = $language;
      $this->cPath = $cPath;
      $this->cPath_array = $cPath_array;

      if ( defined('MODULE_BOXES_CATEGORIES_STATUS') ) {
        $this->sort_order = MODULE_BOXES_CATEGORIES_SORT_ORDER;
        $this->enabled = (MODULE_BOXES_CATEGORIES_STATUS == 'True');
        $this->group = (MODULE_BOXES_CATEGORIES_CONTENT_PLACEMENT == 'Left Column') ? 'boxes_column_left' : 'boxes_column_right';
      }
    }

    public function tep_show_category($counter) {

      for ($i=0; $i<$this->tree[$counter]['level']; $i++) {
        $this->categories_string .= "&nbsp;&nbsp;";
      }

      $this->categories_string .= '<a href="';

      if ($this->tree[$counter]['parent'] == 0) {
        $cPath_new = $counter;
      } else {
        $cPath_new =  $this->tree[$counter]['path'];
      }

      $this->categories_string .= $this->template->getUrl()->hrefLink('blog.read', ['language' => 'en', 'cPath' => $cPath_new]) . '">';

      if (isset($this->cPath_array) && in_array($counter, $this->cPath_array)) {
        $this->categories_string .= '<strong>';
      }

// display category name
      $this->categories_string .= $this->tree[$counter]['name'];

      if (isset($this->cPath_array) && in_array($counter, $this->cPath_array)) {
        $this->categories_string .= '</strong>';
      }

      if (tep_has_category_subcategories($counter)) {
        $this->categories_string .= '-&gt;';
      }

      $this->categories_string .= '</a>';

      if (SHOW_COUNTS == 'true') {
        $products_in_category = tep_count_products_in_category($counter);
        if ($products_in_category > 0) {
          $this->categories_string .= '&nbsp;(' . $products_in_category . ')';
        }
      }

      $this->categories_string .= '<br />';

      if ($this->tree[$counter]['next_id'] != false) {
        $this->tep_show_category($this->tree[$counter]['next_id']);
      }
    }

    public function getData() {

      $this->categories_string = '';

      $categories_query = tep_db_query("select c.categories_id, cd.categories_name, c.parent_id from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.parent_id = '0' and c.categories_id = cd.categories_id and cd.language_id='" . (int)$this->language->get_language()->getId()."' order by sort_order, cd.categories_name");
      while ($categories = tep_db_fetch_array($categories_query))  {
        $this->tree[$categories['categories_id']] = array('name' => $categories['categories_name'],
                                                    'parent' => $categories['parent_id'],
                                                    'level' => 0,
                                                    'path' => $categories['categories_id'],
                                                    'next_id' => false);

        if (isset($parent_id)) {
          $this->tree[$parent_id]['next_id'] = $categories['categories_id'];
        }

        $parent_id = $categories['categories_id'];

        if (!isset($first_element)) {
          $first_element = $categories['categories_id'];
        }
      }

      if (tep_not_null($this->cPath)) {
        $new_path = '';
        foreach($this->cPath_array as $key => $value) {
          unset($parent_id);
          unset($first_id);
          $categories_query = tep_db_query("select c.categories_id, cd.categories_name, c.parent_id from " . TABLE_CATEGORIES . " c, " . TABLE_CATEGORIES_DESCRIPTION . " cd where c.parent_id = '" . (int)$value . "' and c.categories_id = cd.categories_id and cd.language_id='" . (int)$this->language->get_language() ."' order by sort_order, cd.categories_name");
          if (tep_db_num_rows($categories_query)) {
            $new_path .= $value;
            while ($row = tep_db_fetch_array($categories_query)) {
              $this->tree[$row['categories_id']] = array('name' => $row['categories_name'],
                                                   'parent' => $row['parent_id'],
                                                   'level' => $key+1,
                                                   'path' => $new_path . '_' . $row['categories_id'],
                                                   'next_id' => false);

              if (isset($parent_id)) {
                $this->tree[$parent_id]['next_id'] = $row['categories_id'];
              }

              $parent_id = $row['categories_id'];

              if (!isset($first_id)) {
                $first_id = $row['categories_id'];
              }

              $last_id = $row['categories_id'];
            }
            $this->tree[$last_id]['next_id'] = $this->tree[$value]['next_id'];
            $this->tree[$value]['next_id'] = $first_id;
            $new_path .= '_';
          } else {
            break;
          }
        }
      }

      $this->tep_show_category($first_element);

      $data = '<div class="ui-widget infoBoxContainer">' .
              '  <div class="ui-widget-header infoBoxHeading">' . MODULE_BOXES_CATEGORIES_BOX_TITLE . '</div>' .
              '  <div class="ui-widget-content infoBoxContents">' . $this->categories_string . '</div>' .
              '</div>';

      return $data;
    }

    public function execute() {
      global $SID;

      if ((USE_CACHE == 'true') && empty($SID)) {
        $output = tep_cache_categories_box();
      } else {
        $output = $this->getData();
      }

        $this->template->addBlock($output, $this->group);
    }

    public function isEnabled() {
      return $this->enabled;
    }

    public function check() {
      return defined('MODULE_BOXES_CATEGORIES_STATUS');
    }

    public function install() {
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Enable Categories Module', 'MODULE_BOXES_CATEGORIES_STATUS', 'True', 'Do you want to add the module to your shop?', '6', '1', 'tep_cfg_select_option(array(\'True\', \'False\'), ', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, set_function, date_added) values ('Content Placement', 'MODULE_BOXES_CATEGORIES_CONTENT_PLACEMENT', 'Left Column', 'Should the module be loaded in the left or right column?', '6', '1', 'tep_cfg_select_option(array(\'Left Column\', \'Right Column\'), ', now())");
      tep_db_query("insert into " . TABLE_CONFIGURATION . " (configuration_title, configuration_key, configuration_value, configuration_description, configuration_group_id, sort_order, date_added) values ('Sort Order', 'MODULE_BOXES_CATEGORIES_SORT_ORDER', '0', 'Sort order of display. Lowest is displayed first.', '6', '0', now())");
    }

    public function remove() {
      tep_db_query("delete from " . TABLE_CONFIGURATION . " where configuration_key in ('" . implode("', '", $this->keys()) . "')");
    }

    public function keys() {
      return array('MODULE_BOXES_CATEGORIES_STATUS', 'MODULE_BOXES_CATEGORIES_CONTENT_PLACEMENT', 'MODULE_BOXES_CATEGORIES_SORT_ORDER');
    }
  }