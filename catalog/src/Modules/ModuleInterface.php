<?php

namespace Osc\Modules;

use Osc\Services\Template\Template;

interface ModuleInterface {

    public function  execute();

    public function  isEnabled();

    public function  install();

    public function  remove();

    public function  keys();

    public function  check();
}