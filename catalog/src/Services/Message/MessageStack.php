<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2002 osCommerce

  Released under the GNU General Public License

  Example usage:

  $messageStack = new messageStack();
  $messageStack->add('general', 'Error: Error 1', 'error');
  $messageStack->add('general', 'Error: Error 2', 'warning');
  if ($messageStack->size('general') > 0) echo $messageStack->output('general');
*/
namespace Osc\Services\Message;

use Osc\Services\Template\TableBox;

  class MessageStack extends TableBox {
  
  private $messageToStack = [];

// class constructor
    function messageStack() {

      $this->messages = array();

      if (tep_session_is_registered('messageToStack')) {
        $n=sizeof($this->messageToStack);
        for ($i=0; $i<$n; $i++) {
          $this->add($this->messageToStack[$i]['class'], $this->messageToStack[$i]['text'], $this->messageToStack[$i]['type']);
        }
        tep_session_unregister('messageToStack');
      }
    }

// class methods
    public function add($class, $message, $type = 'error') {
      if ($type == 'error') {
        $this->messages[] = array('params' => 'class="messageStackError"', 'class' => $class, 'text' => tep_image(DIR_WS_ICONS . 'error.gif', ICON_ERROR) . '&nbsp;' . $message);
      } elseif ($type == 'warning') {
        $this->messages[] = array('params' => 'class="messageStackWarning"', 'class' => $class, 'text' => tep_image(DIR_WS_ICONS . 'warning.gif', ICON_WARNING) . '&nbsp;' . $message);
      } elseif ($type == 'success') {
        $this->messages[] = array('params' => 'class="messageStackSuccess"', 'class' => $class, 'text' => tep_image(DIR_WS_ICONS . 'success.gif', ICON_SUCCESS) . '&nbsp;' . $message);
      } else {
        $this->messages[] = array('params' => 'class="messageStackError"', 'class' => $class, 'text' => $message);
      }
    }

    public function add_session($class, $message, $type = 'error') {

      if (!tep_session_is_registered('messageToStack')) {
        tep_session_register('messageToStack');
        $this->messageToStack = array();
      }

      $this->messageToStack[] = array('class' => $class, 'text' => $message, 'type' => $type);
    }

    public function reset() {
      $this->messages = array();
    }

    public function output($class) {
      $this->table_data_parameters = 'class="messageBox"';

      $output = array();
      $n=sizeof($this->messages);
      for ($i=0; $i<$n; $i++) {
        if ($this->messages[$i]['class'] == $class) {
          $output[] = $this->messages[$i];
        }
      }

      return $this->tableBox($output);
    }

    public function size($class) {
      $count = 0;

      $n=sizeof($this->messages);
      for ($i=0; $i<$n; $i++) {
        if ($this->messages[$i]['class'] == $class) {
          $count++;
        }
      }

      return $count;
    }
  }
?>
