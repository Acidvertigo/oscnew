<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2014 osCommerce

  Released under the GNU General Public License
*/

namespace Osc\Services\Core;

use Zend\Diactoros\ServerRequest;

  class NavigationHistory {
    private $request;
    private $path = [];
    private $snapshot = [];
    private $self;
    private $get = [];
    private $post = [];
    private $requestType;
    private $cPath = '';

    public function __construct(ServerRequest $request) {
        $this->request = $request;
        $this->self = $this->request->getUri()->getPath();
        $this->get  = $this->request->getQueryParams();
        $this->post = $this->request->getParsedBody();
        $this->requestType = $this->get['request_type'];
        if (isset($this->get['cPath']) && $this->get['cPath'] !== '') {
            $this->cPath = $this->get['cPath'];
        } elseif (isset($this->get['products_id']) && (!isset($this->get['manufacturers_id']))) {
            $this->cPath = tep_get_product_path($this->get['products_id']);
        }
    }
  

    public function add_current_page() {

      $set = 'true';
      $n=sizeof($this->path);
      for ($i=0; $i<$n; $i++) {
        if ($this->path[$i]['page'] == $this->self) {
          if (isset($this->cPath)) {
            if (!isset($this->path[$i]['get']['cPath'])) {
              continue;
            } else {
              if ($this->path[$i]['get']['cPath'] == $this->cPath) {
                array_splice($this->path, ($i+1));
                $set = 'false';
                break;
              } else {
                $old_cPath = explode('_', $this->path[$i]['get']['cPath']);
                $new_cPath = explode('_', $this->cPath);

                $n2=sizeof($old_cPath); 
                for ($j=0; $j<$n2; $j++) {
                  if ($old_cPath[$j] != $new_cPath[$j]) {
                    array_splice($this->path, ($i));
                    $set = 'true';
                    break 2;
                  }
                }
              }
            }
          } else {
            array_splice($this->path, ($i));
            $set = 'true';
            break;
          }
        }
      }

      if ($set == 'true') {
        $this->path[] = array('page' => $this->self,
                              'mode' => $this->requestType,
                              'get'  => $this->filter_parameters($this->get),
                              'post' => $this->filter_parameters($this->post));
      }
    }

    public function remove_current_page() {
      $last_entry_position = sizeof($this->path) - 1;
      if ($this->path[$last_entry_position]['page'] == $this->self) {
        unset($this->path[$last_entry_position]);
      }
    }

    public function set_snapshot($page = '') {

      if (is_array($page)) {
        $this->snapshot = array('page' => $page['page'],
                                'mode' => $page['mode'],
                                'get'  => $this->filter_parameters($page['get']),
                                'post' => $this->filter_parameters($page['post']));
      } else {
        $this->snapshot = array('page' => $this->self,
                                'mode' => $this->requestType,
                                'get'  => $this->filter_parameters($this->get),
                                'post' => $this->filter_parameters($this->post));
      }
    }

    public function clear_snapshot() {
      $this->snapshot = array();
    }

    public function set_path_as_snapshot($history = 0) {
      $pos = (sizeof($this->path)-1-$history);
      $this->snapshot = array('page' => $this->path[$pos]['page'],
                              'mode' => $this->path[$pos]['mode'],
                              'get'  => $this->path[$pos]['get'],
                              'post' => $this->path[$pos]['post']);
    }

    public function debug() {
      $n=sizeof($this->path);
      for ($i=0; $i<$n; $i++) {
        echo $this->path[$i]['page'] . '?';
         foreach($this->path[$i]['get'] as $key => $value) {
          echo $key . '=' . $value . '&';
        }
        if (sizeof($this->path[$i]['post']) > 0) {
          echo '<br />';
          while (list($key, $value) = each($this->path[$i]['post'])) {
            echo '&nbsp;&nbsp;<strong>' . $key . '=' . $value . '</strong><br />';
          }
        }
        echo '<br />';
      }

      if (sizeof($this->snapshot) > 0) {
        echo '<br /><br />';

        echo $this->snapshot['mode'] . ' ' . $this->snapshot['page'] . '?' . tep_array_to_string($this->snapshot['get'], array(tep_session_name())) . '<br />';
      }
    }

    public function filter_parameters(array $parameters) {
      $clean = array();

  //    if (is_array($parameters)) {
        foreach($parameters as $key => $value) {
          if (strpos($key, '_nh-dns') < 1) {
            $clean[$key] = $value;
          }
 //       }
      }

      return $clean;
    }

    public function unserialize($broken) {
      for(reset($broken);$kv=each($broken);) {
        $key=$kv['key'];
        if (gettype($this->$key)!="user function")
        $this->$key=$kv['value'];
      }
    }
    
    public function getCpath() {
       return $this->cPath;
    }
  }
