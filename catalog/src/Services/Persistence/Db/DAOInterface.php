<?php

namespace Osc\Services\Persistence\Db;

interface DAOInterface {

public function findOne($id);

public function findAll();

public function insert(array $data);

public function update(array $data, array $parameters);

}