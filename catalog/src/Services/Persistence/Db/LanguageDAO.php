<?php

namespace Osc\Services\Persistence\Db;

use Doctrine\DBAL\Connection as DBAL;

class LanguageDAO implements DAOInterface {
    
    private $db;

public function __construct(DBAL $db) {
    $this->db = $db;
}

public function findOne($id) {}

public function findAll() {
    $result = $this->db->query("select languages_id, name, code, image, directory from " . TABLE_LANGUAGES . " order by sort_order");
    return $result;
}

public function insert(array $data) {}

public function update(array $data, array $parameters) {}

}