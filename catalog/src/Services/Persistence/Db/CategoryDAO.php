<?php

namespace Osc\Services\Persistence\Db;

use Doctrine\DBAL\Connection as DBAL;

class CategoryDAO implements DAOInterface {
    
    private $db;

    public function __construct(DBAL $db) {
        $this->db = $db;
    }

    public function findOne($id) {}

    public function findAll() {
        $result = $this->db->query("select languages_id, name, code, image, directory from " . TABLE_LANGUAGES . " order by sort_order");
        return $result;
    }
    
    public function countRow($id) {
        $result = $this->db->query("select count(*) as total from " . TABLE_PRODUCTS_TO_CATEGORIES . " where categories_id = '" . (int)$id . "'");
        return $result;
    }
    
    public function countProducts() {

    }

    public function insert(array $data) {}

    public function update(array $data, array $parameters) {}

}