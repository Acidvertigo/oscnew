<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2003 osCommerce

  Released under the GNU General Public License
*/
namespace Osc\Services\Template;

  class Breadcrumb {
    private $_trail = [];

    public function add($title, $link = '') {
      $this->_trail[] = array('title' => $title, 'link' => $link);
    }

    public function trail($separator = ' - ') {
      $trail_string = '';

      $n=sizeof($this->_trail);
      for ($i=0; $i<$n; $i++) {
        if (isset($this->_trail[$i]['link']) && tep_not_null($this->_trail[$i]['link'])) {
          $trail_string .= '<a href="' . $this->_trail[$i]['link'] . '" class="headerNavigation">' . $this->_trail[$i]['title'] . '</a>';
        } else {
          $trail_string .= $this->_trail[$i]['title'];
        }

        if (($i+1) < $n) $trail_string .= $separator;
      }

      return $trail_string;
    }
  }
?>
