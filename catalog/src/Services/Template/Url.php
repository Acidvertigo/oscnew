<?php

namespace Osc\Services\Template;

use Zend\Diactoros\ServerRequest as Request;
use Aura\Router\RouterContainer;

class Url {

private $uri;
private $urlGenerator;

public function __construct( Request $request, RouterContainer $router) {
    $this->uri = $request->getQueryParams();
    $this->urlGenerator = $router->getGenerator();
}

public function hrefLink($route, array $parameters = []) {
    $path = $this->urlGenerator->generate($route, $parameters);
    return htmlspecialchars($path, ENT_QUOTES, 'UTF-8');
}



}