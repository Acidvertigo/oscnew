<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2014 osCommerce

  Released under the GNU General Public License
*/

namespace Osc\Services\Template;

use Osc\Services\i18n\Language;
use League\Container\Container;
use Osc\Services\Template\Breadcrumb;
use Osc\Services\Template\Url;

  class Template {
    private $_title;
    private $_blocks = [];
    private $_content = [];
    private $_grid_container_width = 24;
    private $_grid_content_width = 16;
    private $_grid_column_width = 4;
    private $_data = [];
    private $language;
    private $container;

    public function __construct(Language $language, Container $container, Breadcrumb $breadcrumb, Url $url) {
        $this->_title = TITLE;
        $this->language = $language;
        $this->container = $container;
		$this->breadcrumb = $breadcrumb;
		$this->url = $url;
    }

    public function setGridContainerWidth($width) {
      $this->_grid_container_width = $width;
    }

    public function getGridContainerWidth() {
      return $this->_grid_container_width;
    }

    public function setGridContentWidth($width) {
      $this->_grid_content_width = $width;
    }

    public function getGridContentWidth() {
      return $this->_grid_content_width;
    }

    public function setGridColumnWidth($width) {
      $this->_grid_column_width = $width;
    }

    public function getGridColumnWidth() {
      return $this->_grid_column_width;
    }

    public function setTitle($title) {
      $this->_title = $title;
    }

    public function getTitle() {
      return $this->_title;
    }

    public function addBlock($block, $group) {
      $this->_blocks[$group][] = $block;
    }

    public function hasBlocks($group) {
      return (isset($this->_blocks[$group]) && !empty($this->_blocks[$group]));
    }

    public function getBlocks($group) {
      if ($this->hasBlocks($group)) {
        return implode("\n", $this->_blocks[$group]);
      }
    }

    public function buildBlocks() {

      if ( defined('TEMPLATE_BLOCK_GROUPS') && tep_not_null(TEMPLATE_BLOCK_GROUPS) ) {
        $tbgroups_array = explode(';', TEMPLATE_BLOCK_GROUPS);
        foreach ($tbgroups_array as $group) {
          $module_key = 'MODULE_' . strtoupper($group) . '_INSTALLED';
          if ( defined($module_key) && tep_not_null(constant($module_key)) ) {
            $modules_array = explode(';', constant($module_key));
            foreach ( $modules_array as $module ) {

                $langFile = DIR_WS_LANGUAGES . $this->language->get_language()->getDirectory() . '/modules/' . $group . '/' . $module;
                if ( file_exists($langFile) ) {
                  include($langFile);
                }

                $mb = $this->container->get(ucfirst(basename($module, '.php')));

                if ( $mb->isEnabled() ) {
                  $mb->execute();
                }
            }
          }
        }
      }
    }

    public function addContent($content, $group) {
      $this->_content[$group][] = $content;
    }

    public function hasContent($group) {
      return (isset($this->_content[$group]) && !empty($this->_content[$group]));
    }

    public function getContent($group) {

      if ( !class_exists('tp_' . $group) && file_exists(DIR_WS_MODULES . 'pages/tp_' . $group . '.php') ) {
        include(DIR_WS_MODULES . 'pages/tp_' . $group . '.php');
      }

      if ( class_exists('tp_' . $group) ) {
        $template_page_class = 'tp_' . $group;
        $template_page = new $template_page_class();
        $template_page->prepare();
      }

      foreach ( $this->getContentModules($group) as $module ) {
        if ( !class_exists($module) ) {
          if ( file_exists(DIR_WS_MODULES . 'content/' . $group . '/' . $module . '.php') ) {
            if ( file_exists(DIR_WS_LANGUAGES . $this->language->get_language()->getDirectory() . '/modules/content/' . $group . '/' . $module . '.php') ) {
              include(DIR_WS_LANGUAGES . $this->language->get_language()->getDirectory() . '/modules/content/' . $group . '/' . $module . '.php');
            }

            include(DIR_WS_MODULES . 'content/' . $group . '/' . $module . '.php');
          }
        }

        if ( class_exists($module) ) {
          $mb = new $module;

          if ( $mb->isEnabled() ) {
            $mb->execute();
          }
        }
      }

      if ( class_exists('tp_' . $group) ) {
        $template_page->build();
      }

      if ($this->hasContent($group)) {
        return implode("\n", $this->_content[$group]);
      }
    }

    public function getContentModules($group) {
      $result = array();

      foreach ( explode(';', MODULE_CONTENT_INSTALLED) as $m ) {
        $module = explode('/', $m, 2);

        if ( $module[0] == $group ) {
          $result[] = $module[1];
        }
      }

      return $result;
    }
	
	public function getUrl() {
		return $this->url;
	}
	
	public function getBreadcrumb() {
		return $this->breadcrumb;
	}
  }
?>
