<?php

namespace Osc\Providers;

interface ServiceProviderInterface {

    /**
     * Registers the service(s) with the service container.
     *
     * @param Container $container The service container.
     */
    public function register();
}

