<?php

namespace Osc\Providers;

use League\Container\ServiceProvider\AbstractServiceProvider;
use Zend\Diactoros\ServerRequestFactory;

class App extends AbstractServiceProvider implements ServiceProviderInterface
{
    /**
     * The provides array is a way to let the container
     * know that a service is provided by this service
     * provider. Every service that is registered via
     * this service provider must have an alias added
     * to this array or it will be ignored.
     *
     * @var array
     */
    protected $provides = [
        'Request',
        'Response',
        'Router',
        'shoppingCart',
        'currencies',
        'language',
        'template',
        'messageStack',
        'Breadcrumb',
        'navigation_history',
        'url',
        'db',
        // DAO
        'languageDAO',
        // Mappers
        'category_mapper',
        'language_mapper',
        // Entities
        'category_entities',   
        // Modules
        'Bm_categories.php' ,
        'Bm_manufacturers.php' ,
        'Bm_search.php',
        'Bm_whats_new.php',
        'Bm_information.php',
        'Bm_card_acceptance.php',
        'Bm_shopping_cart.php' ,
        'Bm_manufacturer_info.php' ,
        'Bm_manufacturers',
        'Bm_order_history.php' ,
        'Bm_best_sellers.php' ,
        'Bm_product_notifications.php',
        'Bm_product_social_bookmarks.php',
        'Bm_specials.php' ,
        'Bm_reviews.php' ,
        'Bm_languages.php' ,
        'Bm_currencies.php' ,
        // Header Tags
        'Ht_canonical',
        'Ht_manufacturer_title',
        'Ht_category_title',
        'Ht_product_title',
        'Ht_robot_noindex'
    ];

    /**
     * This is where the magic happens, within the method you can
     * access the container and register or retrieve anything
     * that you need to, but remember, every alias registered
     * within this method must be declared in the `$provides` array.
     */
    public function register()
    {
        $container = $this->getContainer();
        $container->add('Request' , function(array $methods = []) {

            $service = ServerRequestFactory::fromGlobals();

            if (!empty($methods)) {
                foreach ($methods as $method => $args) {
                    $service = $service->$method($args);
                }
            }

            return  $service;
        });
        
        $container->share('Response', 'Zend\Diactoros\Response');
        $container->share('Router', 'Aura\Router\RouterContainer');
        $container->share('shoppingCart', 'Osc\Services\Core\ShoppingCart');
        $container->share('currencies', 'Osc\Services\Currencies\Currencies');
        $container->share('language', 'Osc\Services\i18n\Language')
                  ->withArgument('language_mapper');
        $container->share('template', 'Osc\Services\Template\Template')
                  ->withArgument('language')
                  ->withArgument($container)
          ->withArgument('Breadcrumb')
          ->withArgument('url');
        $container->share('messageStack', 'Osc\Services\Message\MessageStack');
        $container->share('Breadcrumb', 'Osc\Services\Template\Breadcrumb');
        $container->add('navigation_history', 'Osc\Services\Core\NavigationHistory')
                  ->withArgument('Request');
        $container->share('url', 'Osc\Services\Template\Url')
                  ->withArgument('Request')
                  ->withArgument('Router');
                  
        $container->share('db', function() {
            $dbconfig = new \Doctrine\DBAL\Configuration();
            $connectionParams = array(
                'dbname' => DB_DATABASE,
                'user' => DB_SERVER_USERNAME,
                'password' => DB_SERVER_PASSWORD,
                'host' => DB_SERVER,
                'driver' => 'pdo_mysql',
                'charset' => 'UTF8',
            );

                return \Doctrine\DBAL\DriverManager::getConnection($connectionParams, $dbconfig);
            }
          );

      // DAO
        $container->share('languageDAO', 'Osc\Services\Persistence\Db\LanguageDAO')
                       ->withArgument('db');
        $container->share('categoryDAO', 'Osc\Services\Persistence\Db\CategoryDAO')
                       ->withArgument('db');

     // Mappers
        $container->share('category_mapper', 'Osc\Domain\Mappers\CategoryMapper')
                       ->withArgument('categoryDAO');
        $container->share('language_mapper', 'Osc\Domain\Mappers\LanguageMapper')
                        ->withArgument('languageDAO');
     
      // Entities
        $container->add('category_entity', 'Osc\Domain\Entities\CategoryEntity');

      // Modules
        $container->add('Bm_best_sellers', 'Osc\Modules\Boxes\Bm_best_sellers')
                        ->withArgument('template')
                        ->withArgument('language')
                        ->withArgument('Request')
                        ->withArgument('cPath_array');
        $container->add('Bm_card_acceptance', 'Osc\Modules\Boxes\Bm_card_acceptance')
                        ->withArgument('template');
        $container->add('Bm_categories', 'Osc\Modules\Boxes\Bm_categories')
                        ->withArgument('template')
                        ->withArgument('language')
                        ->withArgument('cPath')
                        ->withArgument('cPath_array');
        $container->add('Bm_currencies', 'Osc\Modules\Boxes\Bm_currencies')
                        ->withArgument('template');
        $container->add('Bm_information', 'Osc\Modules\Boxes\Bm_information')
                        ->withArgument('template');
        $container->add('Bm_languages', 'Osc\Modules\Boxes\Bm_languages')
                        ->withArgument('template');
        $container->add('Bm_manufacturer_info', 'Osc\Modules\Boxes\Bm_manufacturer_info')
                        ->withArgument('template')
                        ->withArgument('Request');
        $container->add('Bm_manufacturers', 'Osc\Modules\Boxes\Bm_manufacturers')
                        ->withArgument('template')
                        ->withArgument('Request')
                        ->withArgument('request_type');
        $container->add('Bm_order_history', 'Osc\Modules\Boxes\Bm_order_history')
                        ->withArgument('template');
        $container->add('Bm_product_notifications', 'Osc\Modules\Boxes\Bm_product_notifications')
                        ->withArgument('template');
        $container->add('Bm_product_social_bookmarks', 'Osc\Modules\Boxes\Bm_product_social_bookmarks')
                        ->withArgument('template')
                        ->withArgument('Request')
                        ->withArgument('language');
        $container->add('Bm_reviews', 'Osc\Modules\Boxes\Bm_reviews')
                        ->withArgument('template')
                        ->withArgument('language')
                        ->withArgument('Request');
        $container->add('Bm_search', 'Osc\Modules\Boxes\Bm_search')
                        ->withArgument('template')
                        ->withArgument('Request');
        $container->add('Bm_shopping_cart', 'Osc\Modules\Boxes\Bm_shopping_cart')
                        ->withArgument('template')
                        ->withArgument('Request');
        $container->add('Bm_specials', 'Osc\Modules\Boxes\Bm_specials')
                        ->withArgument('template')
                        ->withArgument('Request')
                        ->withArgument('language')
                        ->withArgument('currencies');
        $container->add('Bm_whats_new', 'Osc\Modules\Boxes\Bm_whats_new')
                        ->withArgument('template')
                        ->withArgument('currencies');
      // Header Tags
        $container->add('Ht_canonical', 'Osc\Modules\Header_tags\Ht_canonical');
        $container->add('Ht_manufacturer_title', 'Osc\Modules\Header_tags\Ht_manufacturer_title');
        $container->add('Ht_category_title', 'Osc\Modules\Header_tags\Ht_category_title');
        $container->add('Ht_product_title', 'Osc\Modules\Header_tags\Ht_product_title');
        $container->add('Ht_robot_noindex', 'Osc\Modules\Header_tags\Ht_robot_noindex');
    }
}