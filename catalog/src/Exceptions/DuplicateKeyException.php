<?php

namespace Osc\Exceptions;

use InvalidArgumentException;

/**
 * Used to indicate that an invalid key was used.
 *
 * @author Kevin Herrera <kevin@herrera.io>
 */
class DuplicateKeyException extends InvalidArgumentException implements ExceptionInterface
{
    /**
     * Returns an exception for an invalid array key.
     *
     * @param string $key The array key.
     *
     * @return OutOfBoundsException The exception.
     */
    public static function duplicateKey($key)
    {
        return new self(sprintf(
            'The array key, %s, is already registered.',
            $key
        ));
    }
}