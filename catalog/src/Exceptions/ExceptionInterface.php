<?php
namespace Osc\Exceptions;
/**
 * Used to indicate that the exception came from this library.
 *
 * @author Kevin Herrera <kevin@herrera.io>
 */
interface ExceptionInterface
{
}