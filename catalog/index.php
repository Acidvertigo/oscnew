<?php

require 'includes/application_top.php';

$server = new Zend\Diactoros\Server(
    function ($request, $response, $done) {
      return  $response;
    },
    $request,
    $response
);

$server->listen();