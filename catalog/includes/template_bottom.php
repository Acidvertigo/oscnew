<?php
/*
  $Id$

  osCommerce, Open Source E-Commerce Solutions
  http://www.oscommerce.com

  Copyright (c) 2010 osCommerce

  Released under the GNU General Public License
*/
?>

</div> <!-- bodyContent //-->

<?php

  if ($this->template->hasBlocks('boxes_column_left')) {
?>

<div id="columnLeft" class="grid_<?php echo $this->template->getGridColumnWidth(); ?> pull_<?php echo $this->template->getGridContentWidth(); ?>">
  <?php echo $this->template->getBlocks('boxes_column_left'); ?>
</div>

<?php
  }

  if ($this->template->hasBlocks('boxes_column_right')) {
?>

<div id="columnRight" class="grid_<?php echo $this->template->getGridColumnWidth(); ?>">
  <?php echo $this->template->getBlocks('boxes_column_right'); ?>
</div>

<?php
  }
?>

<?php require(DIR_WS_INCLUDES . 'footer.php'); ?>

</div> <!-- bodyWrapper //-->

<?php echo $this->template->getBlocks('footer_scripts'); ?>
<?php

?>

</body>
</html>
<?php echo memory_get_usage(); ?>